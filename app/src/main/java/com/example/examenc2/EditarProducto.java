package com.example.examenc2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examenc2.Modelo.ProductoDb;

public class EditarProducto extends AppCompatActivity {

    private Button btnBuscar,btnActualizar, btnCerrar, btnBorrar;
    private EditText txtCodigo, txtNombre, txtMarca, txtPrecio;
    private RadioButton rbtnPerecedero, rbtnNoPerecedero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        iniciarComponentes();

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {buscarProducto();}
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {salir();}
        });

    }

    private void iniciarComponentes() {
        btnBuscar = findViewById(R.id.btnBuscar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbtnPerecedero = findViewById(R.id.rbtnPerecedero);
        rbtnNoPerecedero = findViewById(R.id.rbtnNoPerecedero);
    }

    private void buscarProducto() {
        String codigo = txtCodigo.getText().toString().trim();
        if (!codigo.isEmpty()) {
            ProductoDb productoDb = new ProductoDb(EditarProducto.this);
            Producto producto = productoDb.getProductoPorCodigo(codigo);

            if (producto != null) {
                mostrarDatosProducto(producto);
            } else {
                limpiarCampos();
                mostrarToast("Producto no encontrado");
            }
        } else {
            mostrarToast("Ingrese un código válido");
        }
    }

    private void mostrarDatosProducto(Producto producto) {
        txtNombre.setText(producto.getNombre());
        txtMarca.setText(producto.getMarca());
        txtPrecio.setText(String.valueOf(producto.getPrecio()));

        if (producto.isPerecedero()) {
            rbtnPerecedero.setChecked(true);
        } else {
            rbtnNoPerecedero.setChecked(true);
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Tienda");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    private void limpiarCampos() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbtnPerecedero.setChecked(false);
        rbtnNoPerecedero.setChecked(false);
    }

}
