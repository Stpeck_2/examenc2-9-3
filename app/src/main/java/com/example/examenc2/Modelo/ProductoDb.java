package com.example.examenc2.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examenc2.Producto;

import java.util.ArrayList;

public class ProductoDb implements Persistencia, Proyeccion {

    private Context context;
    private ProductoDbHelper helper;
    private SQLiteDatabase db;

    public ProductoDb(Context context, ProductoDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public ProductoDb(Context context) {
        this.context = context;
        this.helper = new ProductoDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertProducto(Producto producto) {
        String codigo = producto.getCodigo();

        // Verificar si el producto ya existe en la base de datos
        Producto existingProducto = getProductoPorCodigo(codigo);

        ContentValues values = new ContentValues();
        values.put(DefineTable.Productos.COLUMN_NAME_CODIGO, codigo);
        values.put(DefineTable.Productos.COLUMN_NAME_NOMBRE, producto.getNombre());
        values.put(DefineTable.Productos.COLUMN_NAME_MARCA, producto.getMarca());
        values.put(DefineTable.Productos.COLUMN_NAME_PRECIO, producto.getPrecio());
        values.put(DefineTable.Productos.COLUMN_PERECEDERO, producto.isPerecedero() ? 1 : 0);

        this.openDataBase();

        if (existingProducto == null) {
            // Si el producto no existe, insertarlo
            long id = db.insert(DefineTable.Productos.TABLE_NAME, null, values);
            this.closeDataBase();
            return id;
        } else {
            // Si el producto ya existe, actualizarlo
            long id = db.update(DefineTable.Productos.TABLE_NAME, values,
                    DefineTable.Productos.COLUMN_NAME_CODIGO + " = ?",
                    new String[]{codigo});
            this.closeDataBase();
            return id;
        }
    }


    @Override
    public Producto getProducto(int id) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Productos.TABLE_NAME,
                DefineTable.Productos.REGISTRO,
                DefineTable.Productos.COLUMN_NAME_ID + " = ?",
                new String[]{String.valueOf(id)},
                null, null, null);

        Producto producto = null;
        if (cursor.moveToFirst()) {
            producto = readProducto(cursor);
        }

        cursor.close();

        return producto;
    }

    public Producto getProductoPorCodigo(String codigo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Productos.TABLE_NAME,
                DefineTable.Productos.REGISTRO,
                DefineTable.Productos.COLUMN_NAME_CODIGO + " = ?",
                new String[]{codigo},
                null, null, null);

        Producto producto = null;
        if (cursor.moveToFirst()) {
            producto = readProducto(cursor);
        }

        cursor.close();

        return producto;
    }

    @Override
    public ArrayList<Producto> allProductos() {
        this.openDataBase();

        Cursor cursor = db.query(
                DefineTable.Productos.TABLE_NAME,
                DefineTable.Productos.REGISTRO,
                null, null, null, null, null);

        ArrayList<Producto> productos = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Producto producto = readProducto(cursor);
            productos.add(producto);
            cursor.moveToNext();
        }

        cursor.close();

        this.closeDataBase();
        return productos;
    }

    @Override
    public Producto readProducto(Cursor cursor) {
        Producto producto = new Producto();
        producto.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_NAME_ID)));
        producto.setCodigo(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_NAME_CODIGO)));
        producto.setNombre(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_NAME_NOMBRE)));
        producto.setMarca(cursor.getString(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_NAME_MARCA)));
        producto.setPrecio(cursor.getFloat(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_NAME_PRECIO)));
        producto.setPerecedero(cursor.getInt(cursor.getColumnIndexOrThrow(DefineTable.Productos.COLUMN_PERECEDERO)) == 1);
        return producto;
    }
}


//    @Override
//    public ArrayList<Usuario> allUsuarios() {
//        this.openDataBase(); // Abre la base de datos
//
//        Cursor cursor = db.query(
//                DefineTable.Usuarios.TABLE_NAME,
//                DefineTable.Usuarios.REGISTRO,
//                null, null, null, null, null);
//        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
//        cursor.moveToFirst();
//
//        while (!cursor.isAfterLast()){
//            Usuario usuario = readUsuario(cursor);
//            usuarios.add(usuario);
//            cursor.moveToNext();
//        }
//
//        cursor.close();
//
//        this.closeDataBase();
//
//        return usuarios;
//    }